#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    // Complete the code.

    int n;
    cin >> n;

    int m;
    cin >> m;
    /*Firstly we should check first number with a conditional statement. That statement checks the interval.*/
    if (1 <= n <= 9) {
        for (int i = n; i < m+1; i++) {
            if (i == 1) {
                cout << "one" << endl;
            }
            else if (i == 2) {
                cout << "two" << endl;
            }
            else if (i == 3) {
                cout << "three" << endl;
            }
            else if (i == 4) {
                cout << "four" << endl;
            }
            else if (i == 5) {
                cout << "five" << endl;
            }
            else if (i == 6) {
                cout << "six" << endl;
            }
            else if (i == 7) {
                cout << "seven" << endl;
            }
            else if (i == 8) {
                cout << "eight" << endl;
            }
            else if (i == 9) {
                cout << "nine" << endl;
            }    
            else if (i % 2 == 0)
            {
                cout << "even" << endl;
            }
            else
            {
                cout << "odd" << endl;
            }
        }

    }

    else {
        cout << "Please check first number's interval";
    }
    return 0;
}