#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    string s;
    int a, b, c, check_error=0,sum;
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    /*cout<<"Please enter the values for integers (a,b,c) "<<endl;
    cout<<"Rule: 1<=a,b,c<=1000"<<endl;*/


    cin >> a >> b >> c;
    if (a < 1 || a>1000) {
        cout << "Error! int a should be between 0 and 1001" << endl;
        check_error++;
    }


    if (b < 1 || b>1000) {
        cout << "Error! int b should be between 0 and 1001" << endl;
        check_error++;
    }

    if (c < 1 || c>1000) {
        cout << "Error! int c should be between 0 and 1001" << endl;
        check_error++;
    }



    if (check_error != 0) {
        cout << "Error! Total value can be true, result will be below but please check the errors above! " << endl;
    }


    sum = a + b + c;
    cout << sum;


    return 0;
}
