#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

struct Student
{
    int age, standard;
    string first_name, last_name;
};

int main()
{
    Student st_1;

    cin >> st_1.age >> st_1.first_name >> st_1.last_name >> st_1.standard;
    cout << st_1.age << " " << st_1.first_name << " " << st_1.last_name << " " << st_1.standard;

    return 0;
}