#include <fstream>
#include<iostream>
#include <string>
#include<cstdlib>
#include <numeric>  
#include <iomanip>

using namespace std;


#define length 100


int Sum(int arr[], int size)
{
	int sum = 0;
	for (int i = 0; i < size; i++)
	{
		sum = sum + arr[i];
	}
	return sum;
}

int smallest(int arr[], int size) {

	int temp = arr[0];
	for (int i = 0; i < size; i++) {
		if (temp > arr[i]) {
			temp = arr[i];
		}
	}
	return temp;
}

float average(int arr[], int size)
{
	int sum = 0;
	float avg;
	for (int i = 0; i < size; i++)
	{
		sum = sum + arr[i];
	}
	avg = (sum * 1.00) / size;
	return avg;
}

int production(int arr[], int size)
{
	int product = 1;
	for (int i = 0; i < size; i++)
	{
		product *= arr[i];
	}
	return product;
}

int main()
{

	fstream readfile;
	readfile.open("input.txt");
	if (!readfile)
	{
		cout << "Error finding input file" << endl;
		system("pause");
		exit(-1);
	}
	int size;
	int j;
	int s = 0;

	readfile >> size;

	int* a;
	a = new int[size];

	while (!readfile.eof())
	{
		for (int j = 0; j < size; j++)
		{
			readfile >> a[j];
			s++;
		}

	}

	
	readfile.close();


	if (size != s)
	{
		cout << "Attention! There is an ERROR! Please check the format in the text file. Probably results below are not true." << endl;
	}


	cout <<"Sum is:"<< Sum(a, size) << endl;
	cout << "Product is:" << production(a, size) << endl;
	cout << "Average is:" << average(a, size) << endl;
	cout << "Smallest is:" << smallest(a, size) << endl;


	system("pause");
}